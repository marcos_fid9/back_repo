require('../database/connection');
const mongoose = require('mongoose');
const readXlsxFile = require('read-excel-file/node');
const fs = require('fs');

module.exports = {
    load_get(req,res){
        res.render('load_file',{title: 'Express'});
    },
    load_post(req,res){
        fs.writeFileSync('./tempFile.xlsx', req.files.data.data);
        readXlsxFile('./tempFile.xlsx').then((rows) => {
            if(rows != null && rows.length > 1){
                const definition = {};
                for(let key of rows[0]){
                    if(key){
                        definition[key] = String;
                    }else{
                        res.send("No pueden haber ecampos vacios dentro del encabezado de la tabla");
                        return
                    }
                }
                mongoose.models.User = null;
                const infoModel = mongoose.model('User', new mongoose.Schema(definition));
                for(let i in rows){
                    if(i!=0){
                        const userData = {};
                        for(let j in rows[i]){
                            userData[Object.keys(definition)[j]] = rows[i][j];
                        }
                        const user = new infoModel(userData);
                        user.save();
                    }
                }
                res.redirect('/index')
                fs.promises.unlink('./tempFile.xlsx').then(() =>{
                    console.log('File was Deleted')
                })
            }else{
                res.send('DATOS NO VALIDOS EN EL ARCHIVO')
            }
        })
    }
}

