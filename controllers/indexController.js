require('../database/connection');
const mongoose = require('mongoose');
const User = require('../models/user');

module.exports = {
    index_get(req,res){
        User.find({},(err,users) => {
            const definition = {email: String};
            if(users.length > 1){
                for(let user of users){
                    for(key in user._doc)
                        if(key.indexOf("_") != 0)
                            definition[key] = String;
                }
            }
            res.render('index',{ title: 'Express',keys:Object.keys(definition),users});
        })
    }
}

