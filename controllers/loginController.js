require('../database/connection');
const User = require('../models/user');

module.exports = {
    login_get(req,res){
       res.render('login', { title: 'Express' }); 
    },
    login_post(req,res){
        if(req.body.email && req.body.pass){
            const user = new User({email: req.body.email})
            user.save().then(user => res.redirect('/index'))
            .catch(err => res.send('OCURRIO UN ERROR'))
        }else{
            res.send('DATOS NO VALIDOS')
        }
    }

}