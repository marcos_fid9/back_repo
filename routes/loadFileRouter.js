var express = require('express');
var router = express.Router();
var loadFileController = require('../controllers/loadFileController');

/* GET users listing. */
router.get('/', loadFileController.load_get);
router.post('/', loadFileController.load_post)

module.exports = router;
