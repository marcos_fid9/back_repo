var express = require('express');
var router = express.Router();
var indexController = require('../controllers/indexController');

/* GET users listing. */
router.get('/', indexController.index_get);

module.exports = router;
