const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/sherlock', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});